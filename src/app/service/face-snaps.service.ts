import {Injectable} from "@angular/core";
import {FaceSnap} from "../models/face-snap.model";

@Injectable({
  providedIn: "root"
})
export class FaceSnapsService {
  faceSnaps: FaceSnap[] = [
    {
      id: 1,
      title: "Archibald",
      description: "Mon meuilleur ami depuis tout petit",
      imageUrl: "https://cdn.pixabay.com/photo/2015/05/31/16/03/tddy-bear-792273_1280.jpg",
      createDate: new Date(),
      snaps: 0,
      location: "Paris"
    },
    {
      id: 2,
      title: "There Rock Mountain",
      description: "Un endroit magnifique pour les randonnées",
      imageUrl: "https://picsum.photos/300/150?random=1",
      createDate: new Date(),
      snaps: 150,
      location: "le montagne"
    },
    {
      id: 3,
      title: "Un bon repas",
      description: "Mmmh que c'est bon !",
      imageUrl: "https://picsum.photos/300/150?random=2",
      createDate: new Date(),
      snaps: 20
    },
    {
      id: 4,
      title: "Archibald",
      description: "Mon meuilleur ami depuis tout petit",
      imageUrl: "https://cdn.pixabay.com/photo/2015/05/31/16/03/tddy-bear-792273_1280.jpg",
      createDate: new Date(),
      snaps: 0,
      location: "Paris"
    },
    {
      id: 5,
      title: "There Rock Mountain",
      description: "Un endroit magnifique pour les randonnées",
      imageUrl: "https://picsum.photos/300/150?random=1",
      createDate: new Date(),
      snaps: 150,
      location: "le montagne"
    },
    {
      id: 6,
      title: "Un bon repas",
      description: "Mmmh que c'est bon !",
      imageUrl: "https://picsum.photos/300/150?random=2",
      createDate: new Date(),
      snaps: 20
    }
  ];

  getAllFaceSnaps(): FaceSnap[] {
    return this.faceSnaps;
  }

  getFaceSnapById(faceSnapId: number): FaceSnap {
    const faceSnap = this.faceSnaps.find(element => element.id === faceSnapId);
    if (faceSnap) {
      return faceSnap
    } else {
      throw new Error("faceSnap not found !");
    }
  }

  snapFaceSnapById(faceSnapId: number, snapType: 'snap' | 'unsnap'): void {
    const faceSnap = this.getFaceSnapById(faceSnapId);
    snapType === 'snap' ? faceSnap.snaps++ : faceSnap.snaps--;
  }
}
