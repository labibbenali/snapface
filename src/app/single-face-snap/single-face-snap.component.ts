import {Component, OnInit} from '@angular/core';
import {FaceSnap} from "../models/face-snap.model";
import {FaceSnapsService} from "../service/face-snaps.service";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-single-face-snap',
  templateUrl: './single-face-snap.component.html',
  styleUrls: ['./single-face-snap.component.scss']
})
export class SingleFaceSnapComponent implements OnInit {
   faceSnap!: FaceSnap;

  isSnaped!:boolean;
  nameButton!:string;
  constructor(private faceSnapsService:FaceSnapsService,
              private route:ActivatedRoute) {}

  ngOnInit(){
    this.isSnaped=false;
    this.nameButton="Oh Snap !";
    const faceSnapId = +this.route.snapshot.params["id"];
    this.faceSnap=this.faceSnapsService.getFaceSnapById(faceSnapId);
  }
  onSnap(){
    if(!this.isSnaped) {
      this.faceSnapsService.snapFaceSnapById(this.faceSnap.id, "snap");
      this.nameButton = "Oops, un snap !";
      this.isSnaped = true;
    }
    else{
      this.faceSnapsService.snapFaceSnapById(this.faceSnap.id, "unsnap");
      this.nameButton="Oh Snap !";
      this.isSnaped=false;
    }

  }
}
