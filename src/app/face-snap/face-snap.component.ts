import { Component, OnInit, Input } from '@angular/core';
import { FaceSnap } from '../models/face-snap.model';
import {FaceSnapsService} from "../service/face-snaps.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-face-snap',
  templateUrl: './face-snap.component.html',
  styleUrls: ['./face-snap.component.scss']
})

export class FaceSnapComponent implements OnInit{
  @Input() faceSnap!: FaceSnap;

  isSnaped!:boolean;
  nameButton!:string;
  constructor(private faceSnapsService:FaceSnapsService,
              private route: Router){}

  ngOnInit(){
    this.isSnaped=false;
    this.nameButton="Oh Snap !";
  }
  onSnap(){
    if(!this.isSnaped) {
      this.faceSnapsService.snapFaceSnapById(this.faceSnap.id, "snap");
      this.nameButton = "Oops, un snap !";
      this.isSnaped = true;
    }
    else{
      this.faceSnapsService.snapFaceSnapById(this.faceSnap.id, "unsnap");
      this.nameButton="Oh Snap !";
      this.isSnaped=false;
    }

  }

  onViewFaceSnap() {
    this.route.navigateByUrl(`facesnaps/${this.faceSnap.id}`);
  }
}
